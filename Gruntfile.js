var Scripts = require('./config/scripts');
var Styles = require('./config/styles');

module.exports = function(grunt) {

grunt.initConfig({
  concat: {
    home: {
        src: Scripts.home.development,
        dest: "public/build/home.js"
    },
    master: {
        src: Scripts.master.development,
        dest: "public/build/master.js"
    },
    player: {
        src: Scripts.player.development,
        dest: "public/build/player.js"
    },
    stats: {
        src: Scripts.stats.development,
        dest: "public/build/stats.js"
    },
    cssImport: {
      options: {
        process: function(src, filepath) {
          return "@import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin);"+src.replace('@import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin);', '');
        }
      }
    },
    files: {
        "bower_components/semantic/dist/semantic.min.css": ["public/css/semantic.min.css"]
    }
  },
  copy: {
    fonts: {
      expand: true,
      flatten: true,
      src: ['bower_components/font-awesome/fonts/*'],
      dest: 'public/fonts/'
    }
  },
  cssmin: {
    combine: {
      files: {
        'public/build/main.min.css': Styles.layout.grunt
      }
    }
  },
  uglify: {
    home: {
      src: 'public/build/home.js',
      dest: 'public/build/home.min.js'
    },
    master: {
      src: 'public/build/master.js',
      dest: 'public/build/master.min.js'
    },
    player: {
      src: 'public/build/player.js',
      dest: 'public/build/player.min.js'
    },
    stats: {
      src: 'public/build/stats.js',
      dest: 'public/build/stats.min.js'
    }
  },
  jshint: {
    files: [
        '*.js',
        'class/*.js',
        'public/*.js',
        'test/*.js'
    ],
    options: {
      globals: {
        jQuery: true
      },
    },
  },
  watch: {
    main: {
      files: [
        './class/*.js',
        './public/css/*.css',
        './public/js/*.js',
        './test/*.js',
        './views/*/*.html',
        './views/*.html',
        './Gruntfile.js',
        './bower.json',
        './server.js',
      ],
      tasks: ['default'],
      options: {
        spawn: false,
      },
    },
  },
  mochaTest: {
    test: {
      options: {
        reporter: 'spec',
        quiet: false,
        recursive: true,
        timeout: 6000,
        clearRequireCache: false
      },
      src: ['test/*.mocha.js']
    }
  }
});


grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-copy');
grunt.loadNpmTasks('grunt-mocha-test');

grunt.registerTask('test', [ 'mochaTest' ] );
grunt.registerTask('build', [ 'copy', 'cssmin', 'concat', 'uglify' ] );
grunt.registerTask('heroku', [ 'copy', 'cssmin', 'concat', 'uglify' ] );
grunt.registerTask('default', [ 'copy', 'cssmin', 'concat', 'uglify', 'watch' ] );

};
