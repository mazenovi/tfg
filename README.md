# Nightwatch.js Dockerfile for node app

[https://github.com/blueimp/nightwatch](https://github.com/blueimp/nightwatch)

## Usage
Build docker conatiner
```sh
npm build
```

Start the selenium hub, the selenium browser nodes and the app server:
```sh
npm start
```

Run the nightwatch tests:
```sh
npm test
```

Stop and remove the started docker containers:
```sh
npm stop
```

## Debug
Start the debug browser nodes along with the selenium hub and app server:
```sh
docker-compose up -d chromedebug firefoxdebug
```

Connect to the chrome debug node via VNC (password: secret):
```sh
open vnc://$DOCKER_HOST_IP:5900
```

Connect to the firefox debug node via VNC (password: secret):
```sh
open vnc://$DOCKER_HOST_IP:5901
```

Next run the nightwatch tests as shown above.

## License
Released under the [MIT license](http://opensource.org/licenses/MIT).

## Author
[Sebastian Tschan](https://blueimp.net/)
