var Defaults = {
    handSize: 4,
    duration: 600,
    numberOfExpectedPlayer: 4,
    sizePerColor: 13,
};

module.exports = Defaults;