var Rules = {
    DB: {
        name: "Direct Barter",
        visibileHands: true,
        visibileDeck: true,
        wallet: false,
        balance: false,
    },
    Money: {
        name: "Money",
        visibileHands: true,
        visibileDeck: false,
        wallet: true,
        moneyMax: 5,
        moneyMin: 1,
        moneyStep: 1,
        balance: false,
    },
    MC: {
        name: "Mutual Credit",
        visibileHands: true,
        visibileDeck: true,
        wallet: false,
        balance: true,
    }
};

module.exports = Rules;
