scriptslists = [];

socket = [ "/socket.io/socket.io.js" ];

common = [
  "bower_components/jquery/dist/jquery.min.js",
  "bower_components/bootstrap/dist/js/bootstrap.min.js",
  "bower_components/querystring/querystring.min.js",
  "node_modules/underscore/underscore-min.js",
  "public/js/common/init.js",
  "public/js/common/error.js"
];

round = [
  "bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js",
  "bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js",
  "public/js/lib/card.js",
  "public/js/lib/chat.js",
  "public/js/lib/countdown.js",
  "public/js/lib/hand.js",
  "public/js/lib/offer.js",
  "public/js/socket/chat.js",
  "public/js/socket/countdown.js",
  "public/js/socket/hand.js",
  "public/js/socket/offer.js",
  "public/js/socket/round.js",
  "public/js/socket/score.js",
  "public/js/socket/wallet.js",
];

Scripts = {
  home: {
    production : scriptslists.concat([ "/build/home.min.js" ]),
    development : scriptslists.concat(socket, common, [
      "bower_components/handlebars/handlebars.min.js",
      "bower_components/semantic/dist/semantic.min.js",
      "bower_components/jquery-duration-picker/dist/jquery-duration-picker.min.js",
      "public/js/home.js"
    ])
  },
  master: {
    production : scriptslists.concat([ "/build/master.min.js" ]),
    development : scriptslists.concat(socket, common, round, [
      "public/js/master.js"
    ])
  },
  player: {
    production : scriptslists.concat([ "/build/player.min.js" ]),
    development : scriptslists.concat(socket, common, round, [
      "public/js/player.js"
    ])
  },
  stats: {
    production : scriptslists.concat([ "/build/stats.min.js" ]),
    development : scriptslists.concat(common, [
      "bower_components/Chart.js/Chart.min.js",
      "public/js/stats.js"
    ])
  }
}


module.exports = Scripts;
