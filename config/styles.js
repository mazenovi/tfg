styleslists = [
  "/bower_components/bootstrap/dist/css/bootstrap.min.css",
  "/bower_components/bootstrap/dist/css/bootstrap.min.css",
  "/bower_components/bootstrap-material-design/dist/css/ripples.min.css",
  "/bower_components/font-awesome/css/font-awesome.min.css",
  "/bower_components/animate.css/animate.min.css",
  "/bower_components/semantic/dist/semantic.min.css",
  "/bower_components/jquery-duration-picker/dist/jquery-duration-picker.min.css",
  "/bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css",
  "/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
  "/public/css/main.css"
];

// see concat:cssImport in Gruntfile.js
gruntstyleslists = styleslists.slice(0);
gruntstyleslists[styleslists.indexOf("/bower_components/semantic/dist/semantic.min.css")] = "public/css/semantic.min.css";

Styles = {
  layout: {
    production : [ "/public/build/main.min.css" ],
    development : styleslists,
    grunt: gruntstyleslists
  }
}

module.exports = Styles;
