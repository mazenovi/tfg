function Card (params) {
    this.color = params.color;
    this.value = params.value;
}

module.exports = Card;