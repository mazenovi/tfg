function CardGame (params) {
    this.handSize = params.handSize;
    this.numberOfExpectedPlayer = params.numberOfExpectedPlayer;
    this.sizePerColor = params.sizePerColor;
    this.cards = [];
}

CardGame.prototype.generate = function() {
    
    console.log('generate playing card game');
    
    var cardsNumberPerColor = parseInt(this.numberOfExpectedPlayer) + 1;

    for (var i=0; i < this.handSize; i++) {
        if(!this.cards[i] || this.cards[i].constructor !== Array) {
            this.cards[i] = [];
        }
        for (var j=0; j < cardsNumberPerColor; j++) {
            this.cards[i][j] = i + '-' + ( j % this.sizePerColor );
        }
    }

    return this.cards;

};

module.exports = CardGame;