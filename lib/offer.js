var _       = require('underscore');

function Offer (params) {
    this.id = params.id;
    this.offeringPlayer = params.offeringPlayer;
    this.amount = params.amount;
    this.offeredCard = params.offeredCard;
    this.exchangedCard = params.exchangedCard;
    this.acceptingPlayer = '';
}

Offer.prototype.accept = function(acceptingPlayer) {
    this.acceptingPlayer = acceptingPlayer;
    var offeredCardIndex = _.indexOf(this.offeringPlayer.hand, this.offeredCard);
    this.offeringPlayer.hand[offeredCardIndex] = this.exchangedCard;
    this.offeringPlayer.wallet -= Number(this.amount);
    var acceptingCardIndex = _.indexOf(this.acceptingPlayer.hand, this.exchangedCard);
    this.acceptingPlayer.hand[acceptingCardIndex] = this.offeredCard;
    this.acceptingPlayer.wallet += Number(this.amount);
};

Offer.prototype.affectedBy = function(offer) {
    if(
        this.offeredCard == offer.offeredCard ||
        this.offeredCard == offer.exchangedCard ||
        this.exchangedCard == offer.offeredCard ||
        this.exchangedCard == offer.exchangedCard
    ) {
        return true;
    }
    return false;
};

module.exports = Offer;
