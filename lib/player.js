function Player (params) {
    this.id = params.id;
    this.name = params.name;
    this.socketId = params.socketId;
    this.score = 0;
    this.wallet = 0;
    this.hand = [];
}

module.exports = Player;
