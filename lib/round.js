var _ = require('underscore');
var Player   = require('./player');
var CardGame   = require('./card_game');
var Rules  = require('../config/rules');
var Offer   = require('./offer');

function Round (params) {
    this.token = _.random(0, 10000) + '-' + _.now();
    this.name = params.name;
    this.full = false;
    this.forthcoming = true;
    this.inprogress = false;
    this.finished = false;
    this.password = params.password;
    this.handSize = params.handSize;
    this.numberOfExpectedPlayer = params.numberOfExpectedPlayer;
    this.duration = params.duration;
    this.rule = Rules[params.rule];
    var cardGame = new CardGame({
        handSize: params.handSize,
        numberOfExpectedPlayer: params.numberOfExpectedPlayer,
        sizePerColor: params.sizePerColor,
    });
    this.deck = cardGame.generate();
    this.players = [];
    this.offers = [];
    this.offersCounter = 0;
}

Round.prototype.addPlayer = function(params) {
    var player = new Player({
        id: this.players.length,
        name: params.name,
        socketId: params.socketId
    });
    player.hand = this.giveHand();
    player.wallet = this.giveMoney();
    this.players.push(player);
    if(this.numberOfExpectedPlayer <= this.players.length){
        this.full = true;
    }
    return player;
};

Round.prototype.start = function() {
    this.forthcoming = false;
    this.inprogress = true;
};

Round.prototype.stop = function() {
    this.inprogress = false;
    this.finished = true;
};

Round.prototype.getPlayerById = function(id) {
    return this.players[id];
};

Round.prototype.addOffer = function(params) {
    params.id = this.offersCounter++;
    var offer = new Offer(params);
    this.offers.push(offer);
    return offer;
};

Round.prototype.getOfferById = function(id) {
    var offer =  _.findWhere(this.offers, { id: parseInt(id) });
    return offer;
};

Round.prototype.deleteOfferById = function(id) {
    var index = _.findIndex(this.offers, { id: parseInt(id) });
    var offer = _.findWhere(this.offers, { id: parseInt(id) });
    this.offers.splice(index, 1);
    return offer;
};

Round.prototype.giveHand = function() {
    hand = [];
    for (var i=0; i < this.handSize; i++) {
        do
            a = Math.floor(Math.random() * this.deck.length);
        while(this.deck[a].length === 0);
        b = Math.floor(Math.random() * this.deck[a].length);
        hand.push(this.deck[a][b]);
        this.deck[a].splice(b, 1);
    }
    return hand;
};

Round.prototype.giveMoney = function() {
    return this.rule.moneyMax?Math.floor(Math.random() * this.rule.moneyMax):0;
};

Round.prototype.addToDeck = function(hand) {
    hand.forEach(function (card) {
        coord = card.split('-');
        if(this.deck[coord[0]] === undefined || !Array.isArray(this.deck[coord[0]])){
            this.deck[coord[0]] = [];
        }
        this.deck[coord[0]].push(card);
    }, this);
    return this.deck;
};

Round.prototype.winingHand = function(player) {
    _hand = player.hand.map(function (card) {
        coord = card.split('-');
        return coord;
    });
    var squareValue = _hand[0][1];
    var squareValid = true;
    _hand.forEach(function (card) {
        if(card[1] != squareValue) {
            squareValid = false;
        }
    });
    if(squareValid) {
        player.score++;
        this.addToDeck(player.hand);
        player.hand = this.giveHand();
        return true;
    }
    return false;
};

module.exports = Round;
