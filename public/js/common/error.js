var error = {
    password: {
        message: 'invalid password'
    },
    username: {
        message: 'this username already used'
    },
    full: {
        message: 'the room you try to join is already full'
    },
};

if(qs.error) {
    displayError(qs.error);
    if(qs.error === 'password') {
        $.each($('input[name="token"]'), function( ind, elt ) {
            if( $(elt).val() == qs.token ) {
                $(elt).parent().find('input[name="password"]').closest('div').addClass('has-error');
                $(elt).parent().find('input[name="password"]').focus();
            }
        });
    }
}

function displayError(errorCode) {
    $('#errors').addClass('animated bounce');
    $('#errors').show();
    $('#error-messages').prepend('<li>' + error[errorCode].message + '</li>');
}

function hideError() {
    $('#errors').addClass('animated bounce');
    $('#errors').hide();
    $('#error-messages').html();
}
