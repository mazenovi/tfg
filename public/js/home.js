$('.duration').duration_picker({
    lang: 'en'
});

socket.on('round created', function(round, rules){

    round.rules = rules;

    $.get("/views/partials/home/game_form.html", function(resp) {
        compiledTemplate = Handlebars.compile(resp);
        $('#opened-game').prepend(compiledTemplate(round));
        $('#opened-game .duration').duration_picker({
            lang: 'en'
        });
    });

});
