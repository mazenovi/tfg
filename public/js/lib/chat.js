$('#message').submit(function(){
    socket.emit('chat message', $('#playername input[name="name"]').val(), $('#message input').val());
    $('#message input').val('');
    return false;
});
