function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  seconds = seconds > 9 ? seconds : "0" + seconds;
  var minutes = Math.floor( (t/1000/60) % 60 );
  minutes = minutes > 9 ? minutes : "0" + minutes;
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  hours = hours > 9 ? hours : "0" + hours;
  var days = Math.floor( t/(1000*60*60*24) );
  days = days > 9 ? days : "0" + days;
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime){
  var clock = $(id);
  var timeinterval = setInterval(function(){
    var t = getTimeRemaining(endtime);
    clock.html(
        t.days + ':' + t.hours + ':' + t.minutes + ':' + t.seconds
    );
    if(t.total<=0){
      clearInterval(timeinterval);
        socket.emit('finish');
        window.location.replace("/stats?token=" + qs.token);
    }
  },1000);
}
