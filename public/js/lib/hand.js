function displayMyHand(player) {
    return player.hand.map(radioMyCard).join(' ');
}

function radioMyCard(card) {
    _position = card.split('-');
    _radio = '<input type="radio" name="offered-card" value="' + card + '" />';
    if(parseInt(_position[0]) % 2) {
        return '<span class="black">'+ cardsIcons[_position[0]][_position[1]] + _radio + '</span>';
    }
    else {
        return '<span class="red">'+ cardsIcons[_position[0]][_position[1]] + _radio + '</span>';
    }
}

function displayHand(player, rule) {
    _name = '<strong>' + player.name + '</strong><br /> ';
    _hand = player.hand.map(radioCard).join(' ');
    if(rule.wallet) {
      _input = ' <div class="wallet-box"><input type="text" value="1" class="wallet" size="2" /></div> ';
    }
    if(rule.balance) {
      _input = ' <input type="text" value="0" class="balance" /> ';
    }
    _button = '<button class="btn-offer"><i class="fa fa-check-circle-o"></i></button>';
    return _name + _hand + '<span class="handAction">' + _input + _button + '</span>';
}

function radioCard(card) {
    _position = card.split('-');
    _radio = '<input type="radio" name="exchanged-card" value="' + card + '" />';
    if(parseInt(_position[0]) % 2) {
        return '<span class="black">'+ cardsIcons[_position[0]][_position[1]] + _radio + '</span>';
    }
    else {
        return '<span class="red">'+ cardsIcons[_position[0]][_position[1]] + _radio + '</span>';
    }
}

function checkedSelected() {
    $('#compose-offer input[name="exchanged-card"]').closest('li').find('.handAction').hide();
    if($('input[name="offered-card"]:checked').length && $('input[name="exchanged-card"]:checked').length) {
        $('input[name="exchanged-card"]:checked').closest('li').find('.handAction').show();
    }
    else {
        $('input[name="exchanged-card"]:checked').closest('li').find('.handAction').hide();
    }
}
