function hiddenCard(card, status) {
    _position = card.split('-');
    _hidden = '<input type="hidden" name="' + status + '" value="' + card + '" />';
    if(parseInt(_position[0]) % 2) {
        return '<span class="black">'+ cardsIcons[_position[0]][_position[1]] + _hidden + '</span>';
    }
    else {
        return '<span class="red">'+ cardsIcons[_position[0]][_position[1]] + _hidden + '</span>';
    }
}

function card(card) {
    _position = card.split('-');
    if(parseInt(_position[0]) % 2) {
        return '<span class="black">'+ cardsIcons[_position[0]][_position[1]] + '</span>';
    }
    else {
        return '<span class="red">'+ cardsIcons[_position[0]][_position[1]] + '</span>';
    }
}

function cards(cards) {
    return cards.map(card).join(' ');
}

function displayDeck(round) {
    return round.deck.map(cards).join(' ');
}

function displayOffer(offer, action) {

    _name = '<strong>' + offer.offeringPlayer.name + '</strong>';
    _id = '<input type="hidden" name="id" value="' + offer.id + '" /> ';
    _offeredCard = hiddenCard(offer.offeredCard, 'offered-card');
    _exchangedCard = hiddenCard(offer.exchangedCard, 'exchanged-card');
    _amount = '';

    if(offer.amount > 0) {
      _amount = offer.amount + ' <i class="fa fa-money"></i> <input type="hidden" name="offering-amount" value="' + offer.amount + '" /> ';
    }

    if(action == 'accept') {
        _button = '<button class="accept"><i class="fa fa-check-circle-o"></i></button>';
    }
    else {
        _button = '<button class="delete"><i class="fa fa-times-circle-o"></i></button>';
    }
    return _name + '<form class="offer" action="">' + _id + _offeredCard + '<i class="fa fa-hand-o-right"></i>' + _exchangedCard + _amount + _button + '</form>';
}

function offerAlreadyDisplayed(offer) {
    var result = false;
    $('#offers li').each(function () {
        offeredCard = $( this ).find('input[name="offered-card"]').val();
        exchangedCard = $( this ).find('input[name="exchanged-card"]').val();
        if(offer.exchangedCard === exchangedCard && offer.offeredCard === offeredCard)
            result = true;
    });
    return result;
}
