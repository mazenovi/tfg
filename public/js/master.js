/*
 * trigger
 */
if($('#playername input[name="name"]').val()!==''){
    $('#playername input[name="name"]').attr('disabled','disabled');
    $('#playername button').remove();
    $('#message').css('display', 'block');
    socket.emit('master connect', 'master');
}

$('#game-control').submit(function(){
    $('#start-button').addClass('btn disabled');
    socket.emit('play');
    return false;
});

/*
 * listener
 */
socket.on('we can play', function(){
    $('#game-control').html('<button id="start-button"><i class="fa fa-play-circle-o"></i></button>');
});

socket.on('display deck', function(round){
    if($('#deck').length && !$('#deck li').length) {
        $('#deck').append($('<li>').html(displayDeck(round)));
    }
    if($('#deck').length && $('#deck li').length) {
        $('#deck li').html(displayDeck(round));
    }
});

socket.on('wining hand', function(player){
    $('#score').html(parseInt($('#score').html()) + 1);
});
