/*
* trigger
*/
$('#playername').submit(function(){
  socket.emit('new player', $('#playername input[name="name"]').val());
  $('#playername input[name="name"]').attr('disabled','disabled');
  $('title').html($('#playername input[name="name"]').val() + ' - ' + $('title').html());
  $('#playername').find('div').removeClass('has-error');
  $('#playername button').css('display', 'none');
  $('#message').css('display', 'block');
  hideError('username');
  return false;
});

$('#compose-offer').submit(function(){
  _offeringPlayerId = $('#playername input[name="id"]').val();
  _offeredCard = $('input[name="offered-card"]:checked').val();
  _exchangedCard = $('input[name="exchanged-card"]:checked').val();
  _offeringAmount = 0;
  if($('#compose-offer input[name="exchanged-card"]:checked').closest('li').find('input.wallet')){
    _amount = $('#compose-offer input[name="exchanged-card"]:checked').closest('li').find('input.wallet').val();
  }
  socket.emit('new offer', _offeringPlayerId, _amount, _offeredCard, _exchangedCard);
  $('#compose-offer input[name="offered-card"]').attr('checked', false);
  $('#compose-offer input[name="exchanged-card"]').attr('checked', false);
  $('#compose-offer input.wallet').val(1); // TODO set with rule.moneyMin
  $('#compose-offer input[name="exchanged-card"]').closest('li').find('.handAction').hide();
  return false;
});

$('#offers').on('submit', '.offer' , function(){
  _action = $( this ).find('button').attr('class');
  _offerId = $( this ).find('input[name="id"]').val();
  _playerId = $('#playername input[name="id"]').val();
  socket.emit(_action + ' offer', _offerId, _playerId);
  return false;
});

/*
* listener
*/
socket.on('wining hand', function(player){
  if(player.name == $('#playername input[name="name"]').val()) {
    $('#score').html(parseInt(player.score));
  }
});

socket.on('existing player', function(player){
  $('#playername input[name="name"]').addClass('has-error');
  $('#playername input[name="name"]').removeAttr('disabled');
  lastIndex = $('title').html().lastIndexOf(" - ");
  htmlTitle = $('title').html().substr(lastIndex + 3, $('title').html().length - lastIndex);
  $('title').html(htmlTitle);
  $('#playername').find('div').addClass('has-error');
  $('#playername button').css('display', 'inline');
  $('#message').css('display', 'none');
  displayError('username');
});
