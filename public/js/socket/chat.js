socket.on('chat message', function(name, msg){
    $('#messages').prepend($('<li>').html('<strong>' + name + '</strong> : ' + msg));
});

socket.on('new player', function(player){
    if(player.name == $('#playername input[name="name"]').val()) {
        $('#playername').append('<input type="hidden" name="id" value="' + player.id + '" />');
    }
    $('#messages').prepend($('<li>').html('<strong>' + player.name + '</strong> <em>just joined current game</em>'));
});
