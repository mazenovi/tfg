socket.on('display hand', function(player, rule){

  if($('#playername input[name="name"]').val() == player.name && !$('#my-hand li').length) {

    $('#my-hand').append($('<li class="vcenter">').html(displayMyHand(player)));

  }
  else if($('#playername input[name="name"]').val() == player.name && $('#my-hand li').length) {

    $('#my-hand li').html(displayMyHand(player));

  }
  else if(!$('#hands li#' + player.name).length) {

    $('#hands').append($('<li id="' + player.name + '" class="vcenter">').html(displayHand(player, rule)));

    if("wallet" in rule && rule.wallet) {
      $('#hands .wallet').TouchSpin({
        min: rule.moneyMin,
        step: rule.moneyStep,
        postfix: '<i class="fa fa-money"></i>'
      }).on("touchspin.on.startspin", function(){
        var that = this
        $.get("round?token=" + qs.token, function( round ) {
          var player = _.findWhere(round.players, { socketId: '/#' + socket.io.engine.id });
          $(that).trigger("touchspin.updatesettings", {
            max: player.wallet
          });
        });
      });
    }

    if("balance" in rule && rule.balance) {

      $('#hands .balance').slider({
        formatter: function(value) {
          return 'Balance: ' + value;
        },
        min: -2,
        max: 2,
      });
    }

  }
  else {

    $('#hands li#' + player.name).html(displayHand(player, rule));

  }

  $('input[name="offered-card"]').change(checkedSelected);
  $('input[name="exchanged-card"]').change(checkedSelected);

});
