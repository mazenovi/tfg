socket.on('remove offer', function(offer){
    $('#offers li').each(function (index, element) {
        _offeredCard = $(element).find('input[name="offered-card"]').val();
        _exchangedCard = $(element).find('input[name="exchanged-card"]').val();
        if(_offeredCard == offer.offeredCard && _exchangedCard == offer.exchangedCard) {
            $(element).closest('li').remove();
        }
    });
});

socket.on('display offer', function(offer){

    $('#my-hand input[type="radio"]').each(function () {
        if(offer.exchangedCard == $( this ).val() && !offerAlreadyDisplayed(offer)) {
            $('#offers').prepend($('<li>').html(displayOffer(offer, 'accept')));
        }
    });
    if(offer.offeringPlayer.id == $('#playername input[name="id"]').val()) {
        $('#offers').prepend($('<li>').html(displayOffer(offer, 'delete')));
    }
});
