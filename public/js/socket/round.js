socket.on('room full', function(errorCode){
    alert(error[errorCode].message);
    window.location.replace("/");
});

socket.on('lock round', function(round, rules){

    round.rules = rules;

    $.each($('input[name="token"]'), function( ind, elt ) {
        if( $(elt).val() == round.token ) {
            $.get("/views/partials/home/game_form.html", function(resp) {
                compiledTemplate = Handlebars.compile(resp);
                $(elt).parent().replaceWith(compiledTemplate(round));
                $('#opened-game .duration').duration_picker({
                    lang: 'en'
                });
            });
        }
    });

});

socket.on('unlock round', function(round, rules){

    round.rules = rules;

    $.each($('input[name="token"]'), function( ind, elt ) {
        if( $(elt).val() == round.token ) {
            $.get("/views/partials/home/game_form.html", function(resp) {
                compiledTemplate = Handlebars.compile(resp);
                $(elt).parent().replaceWith(compiledTemplate(round));
                $('#opened-game .duration').duration_picker({
                    lang: 'en'
                });
            });
        }
    });

});
