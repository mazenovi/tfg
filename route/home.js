var Round = require('../lib/round.js');

exports.load = function(){

app.route('/')

  .get(function(req, res){
    res.render('home', {
      token: _.random(0, 10000) + '-' + _.now(),
      rounds: rounds,
      rules: Rules,
      defaults: Defaults,
      styles: Styles.layout[process.env.ENV_VARIABLE],
      scripts: Scripts.home[process.env.ENV_VARIABLE]
    });

  })

  .post(function(req, res){
    var round = _.findWhere(rounds, { token: req.body.token });
    if(round) {
        if(round.password === req.body.password) {
            res.redirect(301, '/player?token=' + round.token);
        }
        else {
            if(round.finished) {
                res.redirect(301, '/stats?token=' + round.token);
            }
            else {
                res.redirect(301, '/?error=password&token=' + round.token);
            }
        }
    }
    else {

        var props = {
            token: req.body.token,
            name: req.body.name,
            password: req.body.password,
            handSize: req.body.handSize || Defaults.handSize,
            numberOfExpectedPlayer: req.body.numberOfExpectedPlayer,
            sizePerColor: req.body.sizePerColor || Defaults.sizePerColor,
            duration: req.body.duration,
            rule: req.body.rule,
        };

        round = new Round(props);
        rounds.push(round);
        io.emit('round created', round, Rules);
        res.redirect(301, '/master?token=' + round.token);
    }
  });

}
