exports.load = function(){

app.route('/master')

  .get(function(req, res){
    if(!req.query.token) {
        res.redirect(301, '/');
    }
    else {
        token = req.query.token;
        res.render('master', {
            styles: Styles.layout[process.env.ENV_VARIABLE],
            scripts: Scripts.master[process.env.ENV_VARIABLE]
        });
    }
  });

}
