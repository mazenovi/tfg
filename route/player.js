exports.load = function(){

app.route('/player')

  .get(function(req, res){

    var round = _.findWhere(rounds, { token: req.query.token });

    if(!req.query.token) {
        res.redirect(301, '/');
    }
    else {
        token = req.query.token;
        res.render('player', {
            wallet: round.rule.wallet,
            balance: round.rule.balance,
            styles: Styles.layout[process.env.ENV_VARIABLE],
            scripts: Scripts.player[process.env.ENV_VARIABLE]
        });
    }
  });

}
