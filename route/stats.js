exports.load = function(){

app.route('/stats')

  .get(function(req, res){
    if(!req.query.token) {
        res.redirect(301, '/');
    }
    else {
        token = req.query.token;
        var round = _.findWhere(params.rounds, { token: token });
        res.render('stats', {
            round: round,
            styles: Styles.layout[process.env.ENV_VARIABLE],
            scripts: Scripts.stats[process.env.ENV_VARIABLE]
        });
    }
  });

}
