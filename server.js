_              = require('underscore');
var express    = require('express');
app            = require('express')();
var http       = require('http').Server(app);
io             = require('socket.io')(http);
var bodyParser = require('body-parser');
var exphbs     = require('express-handlebars');

process.env.ENV_VARIABLE = process.env.ENV_VARIABLE?process.env.ENV_VARIABLE:'development';

Rules   = require('./config/rules');
Defaults   = require('./config/defaults');
Scripts   = require('./config/scripts');
Styles   = require('./config/styles');
Round = (process.env.ENV_VARIABLE != "test")?require('./lib/round'):require('./test/roundMock');
rounds  = [];
token   = "";

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));   // to support URL-encoded bodies
app.engine('.html', exphbs({defaultLayout: 'index', extname: '.html'}));
app.set('view engine', '.html');
app.use('/public/', express.static('public'));
app.use('/bower_components/', express.static('bower_components'));
app.use('/node_modules/', express.static('node_modules'));
app.use('/views/partials/', express.static('views/partials'));

var Home = require('./route/home.js');
var Master = require('./route/master.js');
var Player = require('./route/player.js');
var Round = require('./route/round.js');
var Stats = require('./route/stats.js');

Home.load();
Master.load();
Player.load();
Round.load();
Stats.load();

var Chat = require('./socket/chat');
var Round = require('./socket/round');
var Offer = require('./socket/offer');

io.on('connection', function(socket){

  socket.join(token);

  var round = _.findWhere(rounds, { token: token });

  Chat.load(socket);
  Round.load(socket, round, socket.id);
  Offer.load(socket, round, socket.id);

});

// see also http://stackoverflow.com/questions/12236890/run-mocha-tests-in-test-environment
var port = process.env.PORT || 3000;
if(app.get('env') == "test") {
  port = 3002;
}

var server = http.listen(port, function(){
    console.log("Server listening on http://127.0.0.1:" + port);
});

module.exports = server;
