exports.load = function(socket, round){

  socket.on('new offer', function(offeringPlayerId, amount, offeredCard, exchangedCard){
    var offeringPlayer = round.getPlayerById(offeringPlayerId);
    console.log(offeringPlayer.name + ' offered ' + offeredCard + ' against ' +  exchangedCard + ' for ' + amount + ' money in room ' + token);
    params = {
        offeringPlayer: offeringPlayer,
        amount: amount,
        offeredCard: offeredCard,
        exchangedCard: exchangedCard
    };
    var offer = round.addOffer(params);

    io.to(token).emit('display offer', offer);
  });

  socket.on('accept offer', function(offerId, actingPlayerId){

    console.log(offerId + ' - ' + actingPlayerId);

    var actingPlayer = round.getPlayerById(actingPlayerId);
    var offer = round.getOfferById(offerId);
    offer.accept(actingPlayer);

    console.log(offer.offeringPlayer.name + ' offered ' + offer.offeredCard + ' against ' +  offer.exchangedCard + ' and ' + actingPlayer.name + ' accepts in room ' + token);

    [actingPlayer, offer.offeringPlayer].forEach(function (player) {
        while(round.winingHand(player)) {
            io.to(token).emit('wining hand', player);
            io.to(token).emit('display deck', round);
        }
        io.to(token).emit('display hand', player, round.rule);
        io.to(token).emit('display wallet', player);
    });

    round.deleteOfferById(offerId);

    round.offers.forEach(function (_offer, index) {
        if(_offer.affectedBy(offer)) {
            io.to(token).emit('remove offer', round.deleteOfferById(_offer.id));
        }
    });

    io.to(token).emit('remove offer', offer);

  });

  socket.on('delete offer', function(offerId){
    var offer = round.deleteOfferById(offerId);
    console.log(offer.offeringPlayer.name + ' deleted offer with id ' + offers.id + ' in room ' + token);
    io.to(token).emit('remove offer', offer);
  });

}
