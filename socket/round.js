exports.load = function(socket, round, socketId){
  /*
  * init
  */
  socket.on('master connect', function(name){
    console.log('master connected in room ' + token);
    io.to(token).emit('master connect', name);
  });

  socket.on('new player', function(name){
    var existingPlayer = _.findWhere(round.players, { name: name });
    if(!round.full) {
        if(!existingPlayer) {
            console.log('player ' + name  + ' connected in room ' + token);
            io.to(token).emit('new player', round.addPlayer({ name: name, socketId: socketId }));
            if(round.full) {
                console.log('we can play in room ' + token);
                io.to(token).emit('we can play');
                io.emit('lock round', round, Rules);
            }
        }
        else {
            console.log('player ' + name  + ' already connected in room ' + token + '!');
            socket.emit('existing player');
        }
    }
    else {
        console.log('room ' + token + ' is full!');
        socket.emit('room full', 'full');
    }
  });

  /*
  * play
  */
  socket.on('play', function(){
    console.log('display hands in room ' + token);
    round.start();
    io.to(token).emit('reset score');
    io.to(token).emit('start countdown', round.duration);
    round.players.forEach(function (player) {
        while(round.winingHand(player)) {
            io.to(token).emit('wining hand', player);
        }
        io.to(token).emit('display hand', player, round.rule);
        socket.broadcast.to(player.socketId).emit('display wallet', player);
    });
    io.to(token).emit('display deck', round);
  });


  /*
  * finish
  */
  socket.on('finish', function(){
    console.log('game is over in room ' + token);
    io.emit('unlock round', round, Rules);
    round.stop();
  });
}
