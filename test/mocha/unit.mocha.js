var _ = require('underscore');
var assert = require('assert');
var Round   = require('../class/round');

describe('Round', function() {
  
  var handSize = Math.floor(Math.random() * (20 - 4) + 4);
  var numberOfExpectedPlayer = Math.floor(Math.random() * (20 - 4) + 4);

  it('testing for handsize ' + handSize + ' & ' + numberOfExpectedPlayer + ' players');

  round = new Round({
    name: 'testing game',
    handSize: handSize,
    numberOfExpectedPlayer: numberOfExpectedPlayer,
    duration: 1000,
    rules: 'barter' 
  });

  describe('~token', function () {
    it('should have a unique non null token (' + round.token + ')', function () {
      assert.equal(true, round.token.length > 0);
    });
  });

  describe('~deck', function () {
    it('should return a card game with number of colors is the  hand size', function () {
      assert.equal(true, round.deck.length == round.handSize);
    });
    it('should return a card game with as cards value as (number of player + 2) currently ' + round.deck[0].length + ' cards', function () {
      assert.equal(true, round.deck[0].length == round.numberOfExpectedPlayer + 2);
    });
  });

  describe('#winingHand()', function () {
    it('should return true when the hand contains a valid square', function () {
      player = round.addPlayer('user1');
      player.hand = ['A-1','B-1','C-1','D-1'];
      assert.equal(true, round.winingHand(player));
      player.hand = ['A-10','B-10','C-10','D-10'];
      assert.equal(true, round.winingHand(player));
    });
    it('should return false when the hand does not contain a valid square', function () {
      player.hand = ['A-1','B-2','C-3','D-4'];
      assert.equal(false, round.winingHand(player));
      player.hand = ['A-1','A-2','A-3','B-4'];
      assert.equal(false, round.winingHand(player));
    });
  });
});
