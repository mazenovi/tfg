module.exports = {
  'Home page - inspect game for' : function (browser) {
    browser
      .url(browser.launch_url)
      .waitForElementVisible('body', 1000)
      /*
      .source(function (result){
        console.log(result.value);
      })
      */
      browser.expect.element('form#new-form input[name="token"]').to.be.present;
      browser.end();

  },
  'Home page - create new game' : function (browser) {
    browser
      .url(browser.launch_url)
      .setValue('input[name="name"]', 'game1')
      .setValue('input[name="password"]', 'secret')
      .setValue('input[name="numberOfExpectedPlayer"]', 4)
      .click('button.create')
      .window_handles(function(result) {
            var newHandle = result.value[1];
            this.switchWindow(newHandle);
      })
      .assert.attributeEquals('form input[name=name]', 'disabled', 'true')
      .assert.value('form input[name=name]', 'master')
      .end();
  }
};
